class SkyHubImage
  include HTTParty
  base_uri "http://54.152.221.29"

  def images
    response = self.class.get("/images.json")
    response.body.nil? ? {} : JSON.parse(response.body)
  end
end