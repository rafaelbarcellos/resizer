class Image
  include HTTParty
  include Mongoid::Document
  include Mongoid::Paperclip

  has_mongoid_attached_file :attachment,
    styles: { small: '320x240>', medium: "384x288>", large: "640x480>" },
    url: "/images/:style_:basename.:extension"
  validates_attachment_content_type :attachment, content_type: /\Aimage\/.*\z/

  def self.get_all_images
    response = HTTParty.get('http://54.152.221.29/images.json')
    @original_images = response.body.nil? ? {} : JSON.parse(response.body)
  end

  def self.reindex_all_images
    Image.destroy_all
    create_all_formats
  end

  def self.create_all_formats
    @original_images = Image.get_all_images
    return nil unless @original_images['images'].present?

    original_urls.each do |url|
      Image.create(attachment: URI.parse(url))
    end
  end

  def self.original_urls
    @original_images = Image.get_all_images
    @original_images['images'].map { |i| i['url'] }
  end
end
