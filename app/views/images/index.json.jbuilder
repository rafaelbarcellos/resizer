json.array!(@images.each) do |image|
  json.small image.attachment.url(:small)
  json.medium image.attachment.url(:medium)
  json.large image.attachment.url(:large)
end
