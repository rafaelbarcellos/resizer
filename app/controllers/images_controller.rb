class ImagesController < ApplicationController

  def index
    @images = Image.all
    # render json: @images
    respond_to do |format|
      format.json { render 'index' }
    end
  end

  def generate_new_images
    Image.get_all_images
    Image.reindex_all_images
    @images = Image.all
    render json: @images
  end
end
