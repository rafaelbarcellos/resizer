require 'rails_helper'

describe Image do

  describe "get_all_images" do

    it "returns JSON with images webservice", vcr: true do
      VCR.use_cassette(:get_all_images) do
        expect(Image.get_all_images['images'].present?).to be_truthy
      end
    end
  end

  describe "create_all_images" do

    it "create images for each image returned by webservice", vcr: true do
      VCR.use_cassette(:create_all_formats) do
        Image.create_all_formats
      end
      expect(Image.all.count).to eq 10
    end
  end

  describe "reindex_all_images" do

    it "create new images when reindexed", vcr: true do
      VCR.use_cassette(:create_all_formats) do
        Image.create_all_formats
        @old_image = Image.first
      end
      VCR.use_cassette(:reindex_all_images) do
        Image.reindex_all_images
      end
      expect(Image.first).not_to eq @old_image
    end
  end
end