Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :images, only: :index do
    post :generate_new_images, on: :collection
  end
end
