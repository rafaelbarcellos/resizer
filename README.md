# Instructions

1. Clone the repository:

    $ git clone git@bitbucket.org:rafaelbarcellos/resizer.git

    or

    $ git clone  https://rafaelbarcellos@bitbucket.org/rafaelbarcellos/resizer.git

2. Run Bundle to install all gems used on the project

    $ bundle install

3. To run the server application in your terminal: (It should use the port 3000, that is rails default)

    rails s

4. Now you should run the command below to read from webservice and generate the differents formats to each image:

    $ curl --request POST http://localhost:3000/images/generate_new_images

5. To see all image paths and their respective formats, run:

    $ curl http://localhost:3000/images

  6. To run the tests you should run:

      $ rake spec